import { Injectable, OnModuleDestroy, OnModuleInit, Scope, FactoryProvider } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { Connection } from "mysql2/promise";
import { createConnection } from './create-connection';
import { InjectKeys } from './globals';

const DbConnectionProvider : FactoryProvider<Promise<Connection>> = {
    provide: InjectKeys.DB_CONNECTION,

    useFactory: createConnection,

    inject: [ConfigService]
}
export default DbConnectionProvider;