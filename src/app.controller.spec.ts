import { Test, TestingModule } from '@nestjs/testing';
import { ReferalController } from './referal-controller';
import ReferralDb from './referral-db';
import CodesService from './code-service';
import { ConfigService } from '@nestjs/config';
import Mailer from './mailer';
import DbConnectionProvider from './db-connection-provider';

describe('ReferalController', () => {
  let referalController: ReferalController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [ReferalController],
      providers: [ReferralDb, CodesService, ConfigService, Mailer, DbConnectionProvider],
    }).compile();

    referalController = app.get<ReferalController>(ReferalController);
  });

  describe('root', () => {
    it('should generate random str of needed length', () => {
      expect(referalController.GenerateRandomStr(10).length).toBe(10);
    });
  });
});
