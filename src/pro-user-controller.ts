import { Controller, Post, Headers, Body, UnauthorizedException, Query, BadRequestException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import CodesService from './code-service';
import {  ActivatedPromocode, LoginConfirm, PaymentRequestBody, Promocode, ProPayment, ProPaymentWithStringDates, ProPeriod, ProUserData, ReferalWithStats, User, UserReceivingEnd, UserShort, UserShortlWithCodes, UserWithId } from './data-types';
import Mailer from './mailer';
import ProUserDb from './pro-user-db';
import UserController from './user-controller';
import purchaseMailTemplate from './pro-purchase-mail-template';
import trialMailTemplate from './pro-trial-mail-template';
import ReferralDb from './referral-db';
import UserPhoneCheckingService from './user-phone-checking-service';
import ReceiptService from './receipt-service';

@Controller('pro')
export class ProUserController extends UserController {
    constructor(private proUserDB: ProUserDb, private referralDB: ReferralDb, codesService: CodesService, configService: ConfigService, mailer: Mailer, private userPhoneCheckingService: UserPhoneCheckingService, private receiptService: ReceiptService) {
        super(proUserDB, codesService, configService, mailer);
    }
    protected async NotifyAdminAboutRegister(ip: string, data: UserShort) : Promise<void> {
        const body = `IP: ${ip}`
            + `Имя: ${data.name}`
            + `E-mail: ${data.email}`
            + `Телефон: +${data.phone}`;
        await this.mailer.Send(this.configService.get<string>('AdminEmail'), 'Новая регистрация пользователя PRO', body, body);
    }
    protected async NotifyAdminAboutPurchase(payment: ProPaymentWithStringDates, data: UserShort) : Promise<void> {
        const body = `Имя: ${data.name}`
            + `E-mail: ${data.email}`
            + `Телефон: +${data.phone}`
            + `Период: ${payment.name}`
            + `Сумма: ${payment.price}`;
        await this.mailer.Send(this.configService.get<string>('AdminEmail'), 'Покупа PRO', body, body);
    }
    protected GetCodeMailText(): string {
        return 'Ваш код подтверждения для BetMAX PRO'
    }

    @Post('login-confirm')
    async ProUserlLoginConfirm(@Body() data: LoginConfirm) : Promise<{data: UserWithId, token: string}> {
        const userDataWithToken = await super.LoginConfirm(data);
        const {trialAvailable, activeUntil, phoneCheckFails, needLogout} = await this.proUserDB.GetProUserData(userDataWithToken.data.id);
        await this.proUserDB.SetProUserData(userDataWithToken.data.id, trialAvailable, activeUntil, phoneCheckFails, false);
        return userDataWithToken;
    }

    @Post('periods')
    async GetPeriods(@Body() data: LoginConfirm) : Promise<ProPeriod[]> {
        return await this.proUserDB.GetProPeriods();
    }
    @Post('get-pro-user-data')
    async GetProUserData(@Headers('Authorization') token: string) : Promise<ProUserData> {
        const userId = await this.Authorize(token);
        if (userId) {
            const {trialAvailable, activeUntil, phoneCheckFails, needLogout} = await this.proUserDB.GetProUserData(userId);
            const now = new Date();
            const payments = await this.proUserDB.GetPayments(userId);
            const user: ProUserData = {
                trialAvailable,
                activeUntil: activeUntil.toISOString(),
                active: activeUntil > now,
                periodLength: activeUntil <= now ? 0 : (activeUntil.getTime() - now.getTime()) / (1000 * 3600 * 24),
                needLogout,
                payments,
                activatedPromocode: await this.FindCurrentPromocode(userId) || undefined
            }
            return user;
        }
        throw new UnauthorizedException();
    }
    @Post('payment')
    async Payment(@Headers('Authorization') token: string, @Body() requestBody: PaymentRequestBody) {
        const userId = await this.Authorize(token);
        if (userId) {
            let {activeUntil, trialAvailable, phoneCheckFails, needLogout} = await this.proUserDB.GetProUserData(userId);
            let previousPeriodEnd = activeUntil, now = new Date();
            previousPeriodEnd = now > previousPeriodEnd ? now : previousPeriodEnd;
            const newActiveUntil = new Date(previousPeriodEnd.getTime() + 24 * 3600 * 1000 * requestBody.length);
            if (requestBody.isTrial) {
                if (!trialAvailable) {
                    throw new BadRequestException('trialUnavailable');
                }
                trialAvailable = false;
            }
            const user = await this.proUserDB.GetBy(userId, 'id');
            const payment: ProPaymentWithStringDates = {...requestBody, date: now.toISOString(), startsFrom: previousPeriodEnd.toISOString()};
            if (!requestBody.isTrial) {
                await this.SendThanksEmail(user, requestBody.name, requestBody.price);
                if (requestBody.promocode) {
                    const currentPromocode = await this.FindCurrentPromocode(userId);
                    if (!currentPromocode || currentPromocode.code == requestBody.promocode) {
                        const promocode = (await this.proUserDB.GetPromocodes()).find(c => c.code == requestBody.promocode);
                        if (promocode) {
                            if (await this.IsPromocodeAppliable(userId, promocode)) { 
                                payment.promocodeId = promocode.id;
                                await this.MakeReferralPayment(promocode.referralId, this.GetReferralIncome(promocode, requestBody.price));
                            }
                        }
                    }
                }
            } else {
                await this.SendTrialThanksEmail(user, newActiveUntil);
                await this.SchedulePhoneChecking(userId);
            }
            await this.proUserDB.SetProUserData(userId, trialAvailable, newActiveUntil, phoneCheckFails, needLogout);
            const paymentId = await this.proUserDB.InsertPayment({...payment, userId});
            if (!requestBody.isTrial) {
                await this.NotifyAdminAboutPurchase(payment, user);
                //await this.receiptService.SendReceipt(paymentId, payment, user);
            }
        } else {
            throw new UnauthorizedException();
        }
    }
    private GetReferralIncome(promocode: {referralIncome: number, discount: number}, price: number) : number {
        const originalPrice = price / (100 - promocode.discount) * 100;
        return originalPrice * (promocode.referralIncome / 100);
    }
    @Post('phone-check-result')
    async PhoneCheckResult(@Body('phone') phone: string, @Body('status') status: string) {
        const userData = await this.proUserDB.GetBy(phone, UserReceivingEnd.Phone);
        await this.userPhoneCheckingService.SetCheckPhoneResult(userData.id, status == '1');
    }

    @Post('get-promocode-info')
    async GetPromocodeInfo(@Headers('Authorization') token: string, @Body('promocode') code: string) : Promise<{discount: number, trialLengthIncrease: number}> {
        const userId = await this.Authorize(token);
        if (userId) { 
            const promocode = (await this.proUserDB.GetPromocodes()).find(c => c.code == code);
            if (promocode) {
                const currentPromocode = await this.FindCurrentPromocode(userId);
                if (!currentPromocode || currentPromocode.code == code) {
                    if (await this.IsPromocodeAppliable(userId, promocode)) {
                        return {discount: promocode.discount, trialLengthIncrease: promocode.trialLengthIncrease};
                    }
                }
            }
            return {discount: 0, trialLengthIncrease: 0};
        }
        throw new UnauthorizedException();
    }

    @Post('upsert')
    async UpsertProUser(@Headers('X-Forwarded-For') clientIp: string, @Body('data') data: UserShortlWithCodes, @Body('newData') newData?: UserShort) : Promise<{data: User | UserShortlWithCodes, token?: string}> { 
        const res = await super.Upsert(clientIp, data, newData);
        return { data: res.data, token: res.token };
    }

    protected async SendThanksEmail(user: UserShort, periodName: string, periodPrice: number) : Promise<void> {
        const mailHtmlBody = purchaseMailTemplate
            .replace('%username%', user.name)
            .replace('%period_name%', periodName)
            .replace('%period_price%', periodPrice.toString())
            .replace('%current_year%', new Date().getFullYear().toString());
        await this.mailer.Send(user.email, 'BetMAX PRO', `Благодарим Вас за покупку BetMAX PRO.\n\nПериод: ${periodName}.\n\nЦена: ${periodPrice}₽`, mailHtmlBody);
    }

    protected async SendTrialThanksEmail(user: UserShort, endDate: Date) : Promise<void> {
        const trial = (await this.proUserDB.GetProPeriods()).find(p => p.isTrial);
        if (trial) {
            const mailHtmlBody = trialMailTemplate
                .replace('%username%', user.name)
                .replace('%trial_length%', this.FormatDaysForMail(trial.length))
                .replace('%end_date%', this.FormatEndDateForMail(endDate))
                .replace('%current_year%', new Date().getFullYear().toString());
            await this.mailer.Send(user.email, 'BetMAX PRO', `Благодарим Вас за подключение BetMAX PRO.\n\nПериод: ${trial.name}.\n\nЦена: Бесплатно.`, mailHtmlBody);
        }
    }

    private async FindCurrentPromocode(userId: string) : Promise<ActivatedPromocode> {
        const promocodes = await this.proUserDB.GetPromocodes();
        const userPayments = (await this.proUserDB.GetPayments(userId))
            .filter(p => !!p.promocodeId)
            .sort((pa1, pa2) => new Date(pa2.date).getTime() - new Date(pa1.date).getTime());
        const totalApplies: {[promocodeId: number] : number} = {};
        userPayments.forEach(payment => {
            totalApplies[payment.promocodeId] = totalApplies[payment.promocodeId] ? totalApplies[payment.promocodeId] + 1 : 1;
        });

        for (const payment of userPayments) {
            const promocode = promocodes.find(p => p.id == payment.promocodeId);
            if (totalApplies[payment.promocodeId] < promocode.maxApplies || !promocode.maxApplies) {
                return {
                    code: promocode.code,
                    discount: promocode.discount,
                    trialLengthIncrease: promocode.trialLengthIncrease,
                    date: userPayments.reverse().find(p => p.promocodeId == promocode.id).date
                };
            }
        }
        return null;
    }

    private async IsPromocodeAppliable(userId: string, code: Promocode) : Promise<boolean> {
        if (!code.maxApplies) {
            return true;
        } 
        return (await this.proUserDB.GetPayments(userId)).filter(p => !!p.promocodeId && p.promocodeId == code.id).length < code.maxApplies;
    }

    private FormatEndDateForMail(endDate: Date): string {
        const moscowTimezoneOffset = 180 * 60 * 1000;
        const endDateWithMoscowAsUTC = new Date(endDate.getTime() + moscowTimezoneOffset);
        return `${endDateWithMoscowAsUTC.getUTCDate()}.${endDateWithMoscowAsUTC.getUTCMonth()+1}.${endDateWithMoscowAsUTC.getUTCFullYear()} в ${endDateWithMoscowAsUTC.toISOString().substr(11,5)}`;
    }

    private FormatDaysForMail(days: number): string { 
        let daysWord = 'дней';
        if (days % 100 < 10 || days % 100 > 20) {
            const daysLastDigit = days % 10;
            if (daysLastDigit == 1) {
                daysWord = 'день';
            } else if ( daysLastDigit > 1 && daysLastDigit < 5) {
                daysWord = 'дня';
            }
        }
        return `${days} ${daysWord}`;
    }

    private async MakeReferralPayment(referralId: string, sum: number) {
        if (referralId) {
            await this.referralDB.ChangeBalance(referralId, sum * parseFloat(this.configService.get<string>('ReferralBenefit')), '+');
        }
    }

    private async SchedulePhoneChecking(userId: string) {
        const nowTime = new Date().getTime();
        await this.SchedulePhoneCheckingForDate(userId, new Date(nowTime + (1000 * 3600 * 24 * 3)));
        await this.SchedulePhoneCheckingForDate(userId, new Date(nowTime + (1000 * 3600 * 24 * 5)));
    }
    private async SchedulePhoneCheckingForDate(userId: string, date: Date) {
        const usersToCheck = await this.proUserDB.GetUsersToCheckPhones(date);
        if (usersToCheck == null) {
            await this.proUserDB.InsertUsersToCheckPhones(date, userId);
        } else {
            await this.proUserDB.UpdateUsersToCheckPhones(date, usersToCheck + `,${userId}`);
        }
    }
}