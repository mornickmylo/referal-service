import { Body, Controller, Post, Headers, UnauthorizedException } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import AdminService from "./admin-service";
import { ProPayment, UserShortWithIdAndDate } from "./data-types";
import ProUserDb from "./pro-user-db";

@Controller('pro-users')
export default class ProUsersController {
    constructor(private db: ProUserDb, private adminService: AdminService){}

    @Post('login')
    async Login(@Body('login') login: string, @Body('password') password: string) : Promise<{token: string}> {
        const token = this.adminService.Login(login, password);
        if (token) {
            return token;
        } 
        throw new  UnauthorizedException();
    }

    @Post('payments')
    async Payments(@Headers('Authorization') token: string, @Body('userId') userId: string) : Promise<ProPayment[]> {
        if (this.adminService.Authorize(token)) {
            return await this.db.GetPayments(userId);
        }
        throw new  UnauthorizedException();
    }

    @Post('users')
    async Users(@Headers('Authorization') token: string) : Promise<UserShortWithIdAndDate[]> {
        if (this.adminService.Authorize(token)) {
            return await this.db.GetAllUsers();
        }
        throw new  UnauthorizedException();
    }
}