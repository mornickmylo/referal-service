export default `<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html charset=UTF-8" />
        <style>
.container {
    width: 600px;
    background: #EEEEEE;
    font-family: Roboto;
    margin: 0 auto;
}
.top-title-container {
    padding: 28px 0 24px;
    background: #26262B;
    text-align: center;
}
.top-title {
    width: 252px;
    margin: 0 auto;
    display: inline-block;
}
.hello {
    padding-bottom: 5px;
    box-sizing: border-box;
    text-align: center;
    margin-top: 25px;
    font-size: 20px;
}
.hello-text {
    vertical-align: top;
    font-weight: 500;
}
.hello-icon {
    display: inline-block;
    height: 24px;
    min-width: 24px;
    background-size: 28px;
    box-shadow: 0px 3px 4px #00000080;
    background-position: center center;
    border-radius: 14px;
    background-image: url('https://cdn.betmax.ru/dist/icons/cool.png');
}
.container > p {
    text-align: center;
    font-size: 20px;
} 
p.details {
    margin-top: 20px;
    padding: 0px 100px;
}
p.details.first {
    margin-top: 50px;
}
p.thanks {
    margin-bottom: 20px;
    font-weight: 500;
    padding: 0 150px;
}
p.thanks.first {
    margin-top: 80px;
}
.footer {
    background: #26262B;
    margin: 28px auto 0;
    width: 100%;
    padding-top: 15px;
    padding-bottom: 16px;
}
.footer .flex {
    position: relative;
    margin-left: 31px;
    margin-right: 31px;
}
.footer .images .logo {
    width: 120px;
    height: 19px;
    display: inline-block;
}
.footer .images .socials {
    width: 50px;
    display: inline-block;
    margin-left: 361px;
}
.footer .images .socials .icon {
    width: 16px;
    height: 16px;
    cursor: pointer;
    font-size: 0;
    display: inline-block;
    margin-left: 5px;
}
.footer .images .socials .icon.vk {
    width: 20px;
    background: url('https://betmax.ru/img/email/vk.png');
    background-size: 20px;
    background-position: 0 center;
}
.footer .images .socials .icon.vk:hover {
    background: url('https://betmax.ru/img/email/vk-green.png');
    background-size: 20px;
    background-position: 0 center;
}
.footer .images .socials .icon.tg {
    background: url('https://betmax.ru/img/email/telegram.png');
    background-size: 16px;
}
.footer .images .socials .icon.tg:hover {
    background: url('https://betmax.ru/img/email/telegram-green.png');
    background-size: 16px;
}
.footer .images .socials .icon.instagram {
    background: url('https://betmax.ru/img/email/instagram.png');
    background-size: 16px;
}
.footer .images .socials .icon.instagram:hover {
    background: url('https://betmax.ru/img/email/instagram-green.png');
    background-size: 16px;
}
.footer .text {
    margin-top: 6px;
}
.footer .text div, .footer .text a {
    margin: auto 0;
}
.footer .text .we-are-not-bk {
    font-size: 8px;
    color: white;
    display: inline-block;
}
.footer .text .feedback {
    color: #83CA10;
    font-size: 12px;
    font-weight: 500;
    text-decoration: none;
    margin-left: 277px;
}
.footer .you-get-this-mail, .footer .copyright {
    font-size: 8px;
    color: white;
    opacity: 0.5;
    text-align: center;
    margin-top: 15px;
}
.footer .you-get-this-mail a, .footer .copyright a {
    color: white;
}
.footer .copyright {
    margin-top: 8px;
}
.pro-icon {
    width: 74px;
    height: 34px;
    box-shadow: 0 0 8px #f5c719;
    border: 1px solid #f5c719;
    border-radius: 5px;
    background-color: #000000;
    color: #f5c719;
    font-family: Roboto;
    font-size: 32px;
    font-weight: 500;
    padding-top: 5px;
    display: inline-block;
    vertical-align: top;
    margin-top: 5px;
    line-height: 26px;
    cursor: default;
}
        </style>
    </head>
    <body>
        <div class="container">
            <div class="top-title-container">
                <img class="top-title" src="https://betmax.ru/img/email/logo-shadow.png">
                <div class="pro-icon">PRO</div>
            </div>
            <div class="hello">
                <span class="hello-text">Здравствуйте, %username%!</span>
                <div class="hello-icon"></div>
            </div>
            <p class="details first">От Вас поступила оплата за использование BetMAX PRO.</p>
            <p class="details">Тарифный план: <b>%period_name%</b></p>
            <p class="details">Стоимость тарифа: <b>%period_price%₽</b></p>
            <p class="thanks first">Благодарим Вас за подключение BetMAX PRO.</p>
            <p class="thanks">Удачных ставок!</p>
            <div class="footer">
                <div class="images flex">
                    <img class="logo" src="https://betmax.ru/img/logo.png">
                    <div class="socials">
                        <a href="https://vk.com/betmax_ru" target="_blank" class="vk icon"></a>
                        <a href="https://t.me/betmax_ru" target="_blank" class="tg icon"></a>
                    </div>
                </div>
                <div class="text flex">
                    <div class="we-are-not-bk">Мы не являемся букмекерской конторой<br>и не принимаем ставки на спорт.</div>
                    <a class="feedback" href="mailto:admin@betmax.ru">Связаться с нами</a>
                </div>
                <div class="you-get-this-mail">Вы получили данное письмо, потому что зарегистрировались в профессиональной версии расширения BetMAX.<br>Письмо сформировано автоматически. Если у Вас возникнут какие-либо вопросы, напишите в нашу службу поддержки info@betmax.ru</div>
                <div class="copyright">© 2019–%current_year% betmax.ru</div>
            </div>
        </div>
    </body>
</html>`