import { ConfigService } from '@nestjs/config';
import { QueryError, RowDataPacket, Connection, FieldPacket} from 'mysql2/promise';
import { createConnection } from './create-connection';
import { ReferalShort, UserShort, UserReceivingEnd, WithdrawalWithUserData, UserShortWithIdAndDate, Install, AccountType, UserWithId } from './data-types';

export default abstract class Db {
    protected abstract accountType: AccountType;

    private accountTables : {[type in AccountType]: string}= {
        referral: 'referals',
        'pro-user': 'pro-users'
    }
    private createUserProcedures : {[type in AccountType]: string}= {
        referral: 'create_user',
        'pro-user': 'create_pro_user'
    }

    constructor(protected connection : Connection, private configService: ConfigService) {
        console.log(connection);
    }

    private async EnsureOpenedAndQuery<T>(query: string, values?: any) : Promise<[(T & RowDataPacket)[], FieldPacket[]]> {
        try {
            return await this.connection.query<(T & RowDataPacket)[]>(query, values);
        }
        catch {
            this.connection.destroy();
            this.connection = await createConnection(this.configService);
            return this.EnsureOpenedAndQuery<T>(query, values);
        }
    }

    protected async Query<T>(query: string, values?: any) : Promise<T[]> {
        const [rows, fields] = await this.EnsureOpenedAndQuery<T>(query, values);
        const keyedFields: Record<string, FieldPacket> = {};
        fields.forEach(f => keyedFields[f.name] = f);
        return rows.map(r => {
            let res: any = r;
            for (const field in res) {
                if (keyedFields[field].columnType == 0x10 //BIT, see node_modules/mysql2/lib/constants/types.js
                    && keyedFields[field].columnLength == 1) {
                        res[field] = !!res[field][0];
                    }
                else if (res[field] && res[field].getTimezoneOffset) {
                    res[field] = new Date(res[field].getTime() - res[field].getTimezoneOffset() * 60 * 1000)
                } else if (keyedFields[field].columnType == 0xf6 || keyedFields[field].columnType == 0x00) { // DECIMAL
                    res[field] = parseFloat(res[field]);
                }
            }
            return res as T;
        })
    }
    protected async QueryOne<T>(query: string, values?: any) : Promise<T> {
        const rows = await this.Query<T>(query, values);
        if (rows.length) {
            return rows[0];
        }
        return null;
    }
    public async CreateUser(user: UserShort, link?: string): Promise<number> {
        return (await this.QueryOne<{id: number}>(`SELECT ${this.createUserProcedures[this.accountType]}(?, ?, ?, ?) AS id;`, [user.name, user.phone, user.email, link]))?.id;
    }
    public async GetBy(value: string, field: UserReceivingEnd | 'id') : Promise<UserWithId | null> {
        return await this.QueryOne<UserWithId>(`SELECT * FROM \`${this.accountTables[this.accountType]}\` WHERE ${field} = ?;`, [value]);
    }
    public async FindByUserReceivingEnd(value: string): Promise<{user: UserWithId, codeType: UserReceivingEnd}> {
        for (const codeType of Object.values(UserReceivingEnd)) {
            const user = await this.GetBy(value, codeType);
            if (user) {
                return {user, codeType}
            }
        }
        return {user: null, codeType: null};
    }

    public async EditUser(phone: string, newData: UserShort) : Promise<void> {
        await this.connection.execute(`UPDATE \`${this.accountTables[this.accountType]}\` SET name = ?, phone = ?, email = ? WHERE phone = ?;`, [newData.name, newData.phone, newData.email, phone]);
    }
    public async GetCodes(phone: string, email: string) : Promise<{email: string, phone: string} | null> {
        return {email: await this.GetCode(UserReceivingEnd.Email, email), phone: await this.GetCode(UserReceivingEnd.Phone, phone)};
    }
    public async GetCode(type: UserReceivingEnd | 'unknown', value: string) : Promise<string | null> {
        return (await this.QueryOne<{code: string}>(`SELECT code FROM \`referal-codes\` WHERE type = ? AND address = ?;`, [type, value]))?.code;
    }
    public async ConfirmUser(phone: string) {
        await this.connection.execute(`UPDATE \`${this.accountTables[this.accountType]}\` SET confirmed = true WHERE phone = ?;`, [phone]);
    }
    public async SetCode(type: UserReceivingEnd | 'unknown', value: string, login: string) {
        const codeRows = await this.Query(`SELECT * FROM \`referal-codes\` WHERE type = ? AND address = ?;`, [type, login]);
        console.log(codeRows.length);
        if (codeRows.length) {
            console.log(await this.connection.execute(`UPDATE \`referal-codes\` SET code = ? WHERE type = ? AND address = ?;`, [value, type, login]));
        } else {
            console.log(await this.connection.execute(`INSERT INTO \`referal-codes\` SET code = ?, type = ?, address = ?`, [value, type, login]));
        };
    }
    public async InsertWithdrawalRequest(userId: string, sum: number, account: string, accountType: string) : Promise<number> {
        return (await this.QueryOne<{id: number}>(`SELECT insert_withdrawal(?, ?, ?, ?) AS id;`, [userId, sum, account, accountType]))?.id;
    }
    public async GetWithdrawalRequestsWithUserData() : Promise<WithdrawalWithUserData[]> {
        return await this.Query<WithdrawalWithUserData>(`SELECT w.id AS id, r.name AS name, r.phone AS phone, r.email AS email, w.approved AS approved, w.type AS accountType, w.account AS account, w.userId AS userId, w.sum AS sum, w.date AS date, w.rejectReason AS rejectReason FROM \`withdrawal-requests\` AS w LEFT JOIN \`${this.accountTables[this.accountType]}\` AS r ON w.userId = r.id;`);
    }
    public async SetWithdrawalApprove(id: number, approve: boolean) {
        console.log(await this.connection.execute(`UPDATE \`withdrawal-requests\` SET approved = ? WHERE id=?;`, [approve, id]));
    }

    public async SetWithdrawalRejectReason(id: number, rejectReason: string) {
        console.log(await this.connection.execute(`UPDATE \`withdrawal-requests\` SET rejectReason = ? WHERE id=?;`, [rejectReason, id]));
    }

    public async SetLastLogRowId(id: string) {
        console.log(await this.connection.execute(`UPDATE \`last-log-row-id\` SET id = ?;`, [id]));
    }
    public async GetLastLogRowId() : Promise<string> {
        return (await this.QueryOne<{id: string}>(`SELECT id FROM \`last-log-row-id\`;`))?.id
    }
    public async GetUnconfirmedUserId() : Promise<string[]> {
        return (await this.Query<{id: string}>(`SELECT newUserId AS id FROM \`installs\` WHERE confirmed = 0;`))?.map(r => r.id) || [];
    }
    public async GetActionsCounts(userIds: string[]) : Promise<{[userId: string]: number}> {
        return (await this.Query<{count: number, userId: string}>(`SELECT * FROM \`actions-count\` WHERE userId IN (?)`, [userIds]))?.reduce((acc, row) => {
            acc[row.userId] = row.count;
            return acc;
        }, {}) || {};
    }
    public async SetActionsCount(userId: string, count: number) {
        console.log(await this.connection.execute(`REPLACE INTO \`actions-count\` SET userId = ?, count = ?;`, [userId, count]));
    }
    public async ConfirmInstall(userId: string) {
        console.log(await this.connection.execute(`UPDATE \`installs\` SET confirmed = 1 WHERE newUserId = ?;`, [userId]));
        const install = await this.QueryOne<{userId: number}>(`SELECT * FROM \`installs\` WHERE newUserId = ?;`, [userId]);
        if (install) {
            await this.connection.execute(`UPDATE ${this.accountTables[this.accountType]} SET balance = balance + 100 WHERE id=?;`, [install.userId]);
        }
    }
    public async GetAllUsers(): Promise<UserShortWithIdAndDate[]> {
        return await this.Query<UserShortWithIdAndDate>(`SELECT id, name, phone, email, registered AS date FROM \`${this.accountTables[this.accountType]}\`;`);
    }
    public async GetReceiptServiceToken(): Promise<string> {
        return (await this.QueryOne<{token: string}>(`SELECT token FROM \`receipt-service-token\`;`))?.token;
    }
    public async SetReceiptServiceToken(token: string): Promise<void> {
        console.log(await this.connection.execute(`UPDATE \`receipt-service-token\` SET token = ?;`, [token]));
    }
}