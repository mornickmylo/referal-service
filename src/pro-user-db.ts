import { AccountType, InstallStat, Promocode, ProPayment, ProPaymentWithStringDates, ProPeriod, ProUserData } from "./data-types";
import Db from "./db";
import { Inject, Injectable, OnModuleDestroy, OnModuleInit, Scope } from '@nestjs/common';
import { ConfigService } from "@nestjs/config";
import { Connection } from "mysql2/promise";
import { InjectKeys } from './globals';

@Injectable()
export default class ProUserDb extends Db {
    protected accountType = AccountType.ProUser;

    constructor(@Inject(InjectKeys.DB_CONNECTION) protected connection: Connection, configService: ConfigService) {
        super(connection, configService);
    }
    
    public async GetProPeriods(): Promise<ProPeriod[]> {
        return await this.Query<ProPeriod>('SELECT * FROM `pro-periods`;');
    }

    public async GetProUserData(userId: string): Promise<{trialAvailable: boolean, activeUntil: Date, phoneCheckFails: number, needLogout: boolean}> {
        return await this.QueryOne<{trialAvailable: boolean, activeUntil: Date, phoneCheckFails: number, needLogout: boolean}>('SELECT * FROM `pro-data` WHERE userId = ?;', [userId]);
    }
    public async SetProUserData(userId: string, trialAvailable: boolean, activeUntil: Date, phoneCheckFails: number, needLogout: boolean): Promise<void> {
        const res = await this.connection.execute(`UPDATE \`pro-data\` SET trialAvailable = ?, activeUntil = ?, phoneCheckFails = ?, needLogout = ? WHERE userId = ?;`, [trialAvailable, activeUntil.toISOString().replace('T', ' ').replace(/\d{3}Z/, ''), phoneCheckFails, needLogout ? 1 : 0, userId]);
        console.log(res);
    }
    public async GetPayments(userId: string): Promise<ProPayment[]> {
        return await this.Query<ProPayment>('SELECT * FROM `pro-payments` WHERE userId = ?;', [userId]);
    }
    public async InsertPayment(payment: ProPaymentWithStringDates & {userId: string}): Promise<number> {
        return (await this.QueryOne<{id: number}>(`SELECT insert_pro_payment(?, ?, ?, ?, ?, ?, ?, ?) AS id;`, [payment.userId, payment.name, payment.price, payment.length, payment.date.replace('T', ' ').replace(/\d{3}Z/, ''), payment.startsFrom.replace('T', ' ').replace(/\d{3}Z/, ''), payment.isTrial, payment.promocodeId]))?.id;
    }

    public async GetUsersToCheckPhones(date: Date) : Promise<string> {
        return (await this.QueryOne<{userIds: string}>('SELECT userIds FROM `users-to-check-phones` WHERE date = ?;', [date.toISOString().substr(0, 10)]))?.userIds;
    }
    public async InsertUsersToCheckPhones(date: Date, userIds: string) : Promise<void> {
        const res = await this.connection.execute('INSERT INTO `users-to-check-phones` SET userIds = ?, date = ?;', [userIds, date.toISOString().substr(0, 10)]);
        console.log(res);
    }
    public async UpdateUsersToCheckPhones(date: Date, userIds: string) : Promise<void> {
        const res = await this.connection.execute('UPDATE `users-to-check-phones` SET userIds = ? WHERE date = ?;', [userIds, date.toISOString().substr(0, 10)]);
        console.log(res);
    }
    public async GetPromocodes() : Promise<Promocode[]> {
        return await this.Query<Promocode>(`SELECT * FROM promocodes;`);
    }
}