import { Body, Controller, Post, Headers, HttpException, HttpStatus, ConflictException, NotFoundException, UnauthorizedException, Get, Header } from '@nestjs/common';
import Db from './db';
import { LoginConfirm, ReferalWithStats, UserShort, UserShortlWithCodes, UserReceivingEnd, WithdrawalRequest, WithdrawalWithUserData, StatItem, RegisterStatItem } from './data-types';
import CodesService from './code-service';
import Mailer from './mailer';
import ReferralDb from './referral-db';
import { ConfigService } from '@nestjs/config';
import AdminService from './admin-service';

@Controller('withdrawal-controller')
export class WithdrawalController { 
    constructor(private db: ReferralDb, private adminService: AdminService){}
    @Get('withdrawals')
    @Header('Cache-Control', 'no-cache')
    async GetWithdrawals(@Headers('Authorization') token: string): Promise<WithdrawalWithUserData[]> {
        if (this.adminService.Authorize(token)) {
            return await this.db.GetWithdrawalRequestsWithUserData();
        }
        throw new UnauthorizedException();
    }
    @Get('statistics')
    @Header('Cache-Control', 'no-cache')
    async GetStatistics(@Headers('Authorization') token: string): Promise<StatItem[]> {
        if (this.adminService.Authorize(token)) {
            const withdrawals = await this.db.GetWithdrawalRequestsWithUserData();
            const stats: {[userId: number] : StatItem} = {};
            withdrawals.forEach(withdrawal => {
                if (stats[withdrawal.userId]) {
                    if (withdrawal.approved) {
                        stats[withdrawal.userId].approved += withdrawal.sum;
                    } else {
                        stats[withdrawal.userId].disapproved += withdrawal.sum;
                    }
                    stats[withdrawal.userId].total += withdrawal.sum;
                } else {
                    stats[withdrawal.userId] = {
                        email: withdrawal.email,
                        name: withdrawal.name, 
                        id: withdrawal.userId,
                        phone: withdrawal.phone,
                        approved: withdrawal.approved ? withdrawal.sum : 0,
                        disapproved: !withdrawal.approved ? withdrawal.sum : 0,
                        total: withdrawal.sum
                    }
                }
            });
            return Object.values(stats);
        }
        throw new UnauthorizedException();
    }
    @Get('registers')
    @Header('Cache-Control', 'no-cache')
    async GetRegisters(@Headers('Authorization') token: string): Promise<RegisterStatItem[]> {
        if (this.adminService.Authorize(token)) {
            const installs = await this.db.GetAllInstalls();
            const referals = await this.db.GetAllUsers();

            return referals.map(r => {
                const referalInstalls = installs.filter(i => i.userId == r.id);
                return {
                    id: r.id, 
                    name: r.name,
                    phone: r.phone,
                    email: r.email,
                    date: r.date,
                    total: referalInstalls.length,
                    confirmed: referalInstalls.filter(i => i.confirmed).length
                };
            });
        }
        throw new UnauthorizedException();
    }
    @Post('approve')
    async SetApprove(@Headers('Authorization') token: string, @Body('id') id: number, @Body('approve') approve: boolean) {
        if (this.adminService.Authorize(token)) {
            await this.db.SetWithdrawalApprove(id, approve);
        }
    }
    @Post('reject-reason')
    async SetRejectReason(@Headers('Authorization') token: string, @Body('id') id: number, @Body('rejectReason') rejectReason: string) {
        if (this.adminService.Authorize(token)) {
            await this.db.SetWithdrawalRejectReason(id, rejectReason);
        }
    }
    @Post('login')
    async Login(@Body('login') login: string, @Body('password') password: string) : Promise<{token: string}> {
        const token = this.adminService.Login(login, password);
        if (token) {
            return token;
        } 
        throw new  UnauthorizedException();
    }
}