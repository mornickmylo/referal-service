import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { from } from 'rxjs';
import Db from './db';
import { AtolCreateReceiptRequest, AtolCreateReceiptResponse, AtolPaymentType, LogItem, ProPayment, ProPaymentWithStringDates, TaxType, User } from './data-types';
import axios, {AxiosResponse, AxiosError} from 'axios';
import Mailer from './mailer';
import ReferralDb from './referral-db';
import { ConfigService } from '@nestjs/config';
import { stringify } from 'querystring';
import ProUserDb from './pro-user-db';


@Injectable()
export default class ReceiptService {
    constructor(private db: ProUserDb, private configService: ConfigService) {}

    public async SendReceipt(paymentId: number, payment: ProPaymentWithStringDates, user: User) {
        const url = `${this.configService.get('ReceiptServiceGroupCode')}/sell`;
        const request: AtolCreateReceiptRequest = {
            timestamp: payment.date.replace('T', ' ').replace(/\.\d{3}Z/, ''),
            external_id: paymentId.toString(),
            receipt: {
                client: {
                    email: user.email,
                    phone: '+'+user.phone,
                    name: user.name
                },
                company: {
                    email: this.configService.get<string>('AdminEmail'),
                    inn: this.configService.get<string>('ReceiptServiceINN'),
                    payment_address: this.configService.get<string>('ReceiptServicePaymentAddress'),
                },
                payments: [
                    {
                        type: AtolPaymentType.Cashless,
                        sum: payment.price
                    }
                ],
                total: payment.price,
                items: [{
                    name: `BetMAX PRO, ${payment.name}`,
                    price: payment.price,
                    quantity: 1,
                    sum: payment.price,
                    vat: {
                        type: TaxType.none
                    }
                }]
            }
        };
        console.log(request);
        const resp = await this.SendAuthorized<AtolCreateReceiptResponse>(url, request);
        console.log(resp);
        if (resp.error) {
            throw new Error(resp.error.text);
        }
    }

    private async SendAuthorized<T>(relativeUrl: string, body: object) : Promise<T> {
        let token = await this.db.GetReceiptServiceToken();

        let response = await this.Send<T>(relativeUrl, body, token);
        if (response.status == 401) {
            token = await this.UpdateToken();
            response = await this.Send<T>(relativeUrl, body, token);
            if (response.status == 401) {
                throw new Error('Request failed even after token update');
            }
        }
        return response.data;
    }

    private async Send<T>(relativeUrl: string, body: object, token?: string): Promise<AxiosResponse<T>> {
        const headers = token ? {'Token': token} : undefined;
        try {
            return await axios.post<T>(this.configService.get<string>('ReceiptServiceUrl')+relativeUrl, body, {headers});
        }
        catch (e: any) {
            return (e as AxiosError<T>).response;
        }
    }

    private async UpdateToken(): Promise<string> {
        const tokenResult = await this.Send<{token: string, error: string}>('getToken', {
            login: this.configService.get<string>('ReceiptServiceLogin'),
            pass: this.configService.get<string>('ReceiptServicePassword'),
        });
        if (tokenResult.status != 200) {
            throw new Error(tokenResult.data.error);
        };
        await this.db.SetReceiptServiceToken(tokenResult.data.token);
        return tokenResult.data.token;
    }
}