import { AccountType, Install, InstallStat, LinkAndBalance, Promocode, ProPayment, PurchaseStat, ReferalExtended, User, UserReceivingEnd, UserShortWithIdAndDate, UserWithId, WithdrawalHistory, WithdrawalHistoryItem } from "./data-types";
import Db from "./db";
import { Inject, Injectable, Scope } from '@nestjs/common';
import { ConfigService } from "@nestjs/config";
import { Connection } from "mysql2/promise";
import { InjectKeys } from "./globals";

@Injectable()
export default class ReferralDb extends Db {
    protected accountType = AccountType.Referral;

    constructor(@Inject(InjectKeys.DB_CONNECTION) protected connection: Connection, configService: ConfigService) {
        super(connection, configService);
    }

    public async GetLinkAndBalance(userId: string) : Promise<LinkAndBalance> { 
        return await this.QueryOne<LinkAndBalance>('SELECT balance, link FROM `referals` WHERE id = ?;', [userId]);
    }
    public async GetWithdrawalHistory(userId: string) : Promise<WithdrawalHistoryItem[]> { 
        const withdrawalHistory = await this.Query<Omit<WithdrawalHistoryItem, 'sum'> & {sum: string}>('SELECT account, type, approved, date, rejectReason, sum FROM `withdrawal-requests` WHERE userId = ?;', [userId]);
        return withdrawalHistory.map(rdp => {
            return {
                ...rdp,
                sum: parseFloat(rdp['sum'])
            }
        });
    }
    public async GetPromocode(userId: string) : Promise<Promocode> { 
        return await this.QueryOne<Promocode>('SELECT * FROM `promocodes` WHERE referralId = ?;', [userId]);
    }
    public async GetStats(userId: string) : Promise<{clicks: string[], installs: InstallStat[]}> {
        const data : {clicks: string[], installs: InstallStat[]} = {
            clicks: [],
            installs: []
        }
        let [rows, fields] = await this.connection.query(`SELECT date FROM clicks WHERE userId = ?;`, [userId]);
        const clickRows = rows as {date: Date}[];
        if (clickRows.length) {
            console.log(typeof clickRows[0].date.getTime);
            data.clicks = Array.from(clickRows).map(c => c.date.toISOString());
        }
        [rows, fields] = await this.connection.query(`SELECT date, confirmed FROM installs WHERE userId = ?;`, [userId]);
        const installRows = rows as {date: Date, confirmed: number[]}[];
        if (installRows.length) {
            data.installs = Array.from(installRows).map(c => { 
                return {
                    date: c.date.toISOString(),
                    confirmed: !!c.confirmed[0]
                }
            });
        }
        return data;
    }
    public async GetPurchaseStats(userId: string) : Promise<PurchaseStat[]> {
        const rows = await this.Query<PurchaseStat & {discount: number, price: number, referralIncome: number}>(`SELECT purchase.name AS name, purchase.price AS price, purchase.date AS date, promocode.discount AS discount, promocode.referralIncome AS referralIncome FROM \`pro-payments\` AS purchase INNER JOIN promocodes AS promocode ON purchase.promocodeId IS NOT NULL AND purchase.promocodeId = promocode.id WHERE promocode.referralId = ?`, [userId]);
        return rows.map(r => {
            const userPayed = r.price, incomePercent = r.referralIncome;
            return {
                name: r.name,
                income: (userPayed / (100 - r.discount)) * incomePercent,
                date: r.date
            };
        });
    }
    public async ChangeBalance(userId: string, sum: number, operation: '+' | '-') {
        console.log(await this.connection.execute(`UPDATE \`referals\` SET balance = balance ${operation} ? WHERE id=?;`, [sum, userId]));
    }
    public async GetReferralIdByUserId(userId: string) : Promise<number> {
        return (await this.QueryOne<{referralId: number}>(`SELECT userId AS referralId FROM installs WHERE newUserId = ?`, [userId]))?.referralId;
    }
    public async GetAllInstalls(): Promise<Install[]> {
        return await this.Query<Install>(`SELECT * FROM \`installs\`;`);
    }
    public async GetInstallPrice() : Promise<number> {
        return (await this.QueryOne<{price: number}>(`SELECT price FROM \`install-price\`;`))?.price || 100;
    }
    public async CheckLinkExists(link: string) : Promise<boolean> {
        const userRows = await this.Query(`SELECT link FROM \`referals\` WHERE link LIKE ?;`, ['%'+link]);
        return !!userRows.length;
    }
    public async InsertPromocode(promocode: Omit<Promocode, 'id'>) : Promise<void> {
        await this.connection.execute(`INSERT INTO \`promocodes\` SET referralId = ?, code = ?, discount = ?, referralIncome = ?, trialLengthIncrease = ?, maxApplies = ?;`, [promocode.referralId, promocode.code, promocode.discount, promocode.referralIncome, promocode.trialLengthIncrease, promocode.maxApplies]);
    }
}