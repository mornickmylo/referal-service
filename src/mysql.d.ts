import * as mysql2 from 'mysql2/promise';

declare module 'mysql2/promise' {
    export interface FieldPacket {
        columnType: number,
        columnLength: number
    }
}
