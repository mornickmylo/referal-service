import { type } from "os"

export interface UserShort {
    name: string,
    phone: string,
    email: string,
}
export interface User extends UserShort {
    registered: string,
    confirmed: boolean,
}
export interface LinkAndBalance {
    link: string,
    balance: number,
}
export interface ReferralPromocode {
    promocode: Promocode
}
export interface PurchaseStat {
    name: string, 
    income: number,
    date: Date
}
export interface ReferralStats {
    clicks: string[],
    installs: InstallStat[],
    purchases: PurchaseStat[]
}
export interface WithdrawalHistory {
    withdrawalHistory: WithdrawalHistoryItem[]
}
export interface InstallStat {
    date: string,
    confirmed: boolean
}
export interface Id {
    id: string,
}
export enum UserReceivingEnd {
    Phone = 'phone',
    Email = 'email'
};
export interface Codes {
    codes: {[type in UserReceivingEnd] : string}
}
export type ReferalShort = User & LinkAndBalance;
export type ReferalWithWithdrawals = User & LinkAndBalance & WithdrawalHistory;
export type ReferalWithStats = User & LinkAndBalance & WithdrawalHistory & ReferralStats;
export type UserWithId = User & Id;
export type ReferalExtended = User & LinkAndBalance & WithdrawalHistory & Id;
export type UserShortlWithCodes = UserShort & Codes;
export type ReferralDataWithStats = LinkAndBalance & WithdrawalHistory & ReferralStats & ReferralPromocode;

export interface Install extends InstallStat {
    ip: string, 
    userId: string,
    newUserId: string
}

export interface LoginConfirm {
    code: string,
    login: string
}


export interface Security {
    login: string;
    password: string;
}

export interface Abonent {
    phone: string;
    number_sms: string;
    client_id_sms?: string;
    time_send?: string;
    validity_period?: string;
}

export interface Message {
    type: string;
    sender: string;
    text: string;
    name_delivery: string;
    translite: string;
    abonent: Abonent[];
}

export interface SmsRequest {
    security: Security;
    type: string;
    message: Message[];
}
export interface SmsInfo {
    number_sms: string,
    id_sms: string,
    parts: string,
    action: string
}
export interface SmsResponse {
    sms: SmsInfo[]
}
export interface CallResponse {
    status: boolean, // true в случае успеха, false в случае неудачи
    ucaller_id: number, // уникальный ID в системе uCaller, который позволит проверять статус и инициализировать метод initRepeat
    phone: number, // номер телефона, куда мы совершили звонок
    code: number, // код, который будет последними цифрами в номере телефона
    client: string, // идентификатор пользователя переданный клиентом
    unique_request_id: string, // появляется только если вами был передан параметр `unique`
    exists: boolean // появляется при переданном параметре `unique`, если такой запрос уже был инициализирован ранее
}
export interface WithdrawalRequest {
    phone: string,
    accountType: string,
    account: string,
    sum: number
}
export enum WithdrawalRequestStatus {
    Pending,
    Approved,
    Rejected
}
export interface WithdrawalHistoryItem {
    account: string,
    type: 'qiwi' | 'card' | 'bitcoin',
    approved: WithdrawalRequestStatus,
    date: string,
    rejectReason: string,
    sum: number
}
export interface WithdrawalWithUserData {
    id: number,
    name: string,
    phone: string,
    email: string,
    approved: boolean,
    accountType: string,
    account: string,
    sum: number,
    userId: number,
    date: string
}
export interface StatItem {
    id: number,
    name: string,
    phone: string,
    email: string,
    approved: number,
    disapproved: number,
    total: number
}
export interface LogItem {
    date: Date,
    userId: string,
    ip: string,
    action: string,
    bk_name: string,
    bk_event_id: string,
    request_id: string,
    bet: string,
    prematch: boolean,
    created_at: Date,
    updated_at: Date
}
export interface RegisterStatItem {
    id: string,
    name: string,
    phone: string,
    email: string,
    total: number,
    confirmed: number,
    date: string
}
export interface UserShortWithIdAndDate extends UserShort {
    id: string,
    date: string
}
export enum AccountType {
    Referral = 'referral',
    ProUser = 'pro-user'
}
export interface ProPeriod {
    name: string,
    price: number,
    discount: number,
    length: number,
    isTrial: boolean
}
export interface ProPayment {
    name: string,
    price: number,
    length: number,
    date: Date,
    startsFrom: Date,
    isTrial: boolean,
    promocodeId?: number
}
export type ProPaymentWithStringDates = Omit<ProPayment, 'date' | 'startsFrom'> & {date: string, startsFrom: string}
export interface ProUserData {
    trialAvailable: boolean,
    activeUntil: string,
    active: boolean,
    periodLength: number,
    needLogout: boolean,
    payments: ProPayment[],
    activatedPromocode?: ActivatedPromocode
}
export enum TaxSystem {
    "osn" = "osn",
    "usn_income" = "usn_income",
    "usn_income_outcome" = "usn_income_outcome",
    "envd" = "envd",
    "esn" = "esn",
    "patent" = "patent"
}
export enum AtolPaymentType {
    Cash = 0,
    Cashless = 1,
    Prepayment = 2,
    Credit = 3,
    Other = 4
}
export interface AtolPayment {
    type: AtolPaymentType,
    sum: number
}
export interface AtolItem {
    name: string,
    price: number,
    quantity: number,
    sum: number,
    vat: TaxInfo
}
export enum TaxType {
    "none" = "none",
    "vat0" = "vat0",
    "vat10" = "vat10",
    "vat18" = "vat18",
    "vat110" = "vat110",
    "vat118" = "vat118",
    "vat20" = "vat20",
    "vat120" = "vat120"
}
export interface TaxInfo {
    type: TaxType,
    sum?: number
}
export interface AtolCreateReceiptRequest {
    timestamp: string,
    external_id: string,
    service?: {
        callback_url: string
    },
    receipt: {
        client: {
            email: string,
            phone?: string,
            name?: string,
            inn?: string
        },
        company: {
            email: string,
            sno?: TaxSystem,
            inn: string,
            payment_address: string
        },
        agent_info?: object,
        supplier_info?: object,
        payments: AtolPayment[],
        total: number,
        items: AtolItem[],
        vats?: Required<TaxInfo>[]
    }
}
export interface AtolCreateReceiptError {
    "error_id": string,
    "code": number,
    "text": string,
    "type": string
}
export interface AtolCreateReceiptResponse {
    "uuid"?: string,
    "timestamp": string,
    "error": null | AtolCreateReceiptError,
    "status": string,
}
export type PaymentRequestBody = ProPeriod & {
    extensionUserId: string, 
    promocode?: string, 
    promocodeDate?: string
}
export interface Promocode {
    id: number,
    referralId: string,
    code: string,
    discount: number,
    referralIncome: number,
    trialLengthIncrease: number,
    maxApplies: number
}
export interface ActivatedPromocode {
    discount: number,
    code: string,
    trialLengthIncrease: number,
    date: Date
}