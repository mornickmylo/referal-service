import { BadRequestException, Body, Controller, Get, Post, Query, Req } from '@nestjs/common';
import { Request } from 'express';
import axios from 'axios';
import { ConfigService } from '@nestjs/config';
import RegisterCountingService from './register-counting-service';
import { LogItem } from './data-types';

@Controller('log')
export class LoggingController {
    constructor(private configService: ConfigService, private registerCountingService: RegisterCountingService) {}
    @Post('log')
    async Log(@Body('stack') callStack: string, @Req() req: Request) {
        const clientIp = req.headers['x-forwarded-for'] || req.socket.remoteAddress;
        console.log(`${new Date().toLocaleString('ru-RU')}: got stack logging request from ${clientIp}: ${callStack}`);
    }
    @Get('service-up')
    async IsLoggingServiceUp() : Promise<string> {
        const res = await this.registerCountingService.IsLoggingServiceUp(false);
        return res ? 'true' : 'false';
    }
    @Get('logs-after')
    async GetLogsAfter(@Query('date') dateStr: string) : Promise<LogItem[]> {
        const date = new Date(dateStr);
        if (date && date < new Date()) {
            return await this.registerCountingService.GetLogRowsFrom(date);
        }
        throw new BadRequestException("Неверная дата. Дата должна быть раньше сейчас и иметь правильный формат");
    }
}