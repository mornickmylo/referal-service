import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import {sign, verify} from 'jsonwebtoken';


@Injectable()
export default class AdminService {
    constructor(private configService: ConfigService) {}

    public GetToken(login: string, password: string) {
        return sign({date: new Date().toISOString(), login, password}, this.configService.get<string>('TokenSign'));
    }
    public Authorize(token: string) : boolean {
        try {
            const data = verify(token, this.configService.get<string>('TokenSign')) as {date: string, login: string, password: string};
            return new Date(new Date().getTime() - 3600*1000*3) < new Date(data.date) && data.login == this.configService.get<string>('AdminLogin') && data.password == this.configService.get<string>('AdminPassword');
        } catch(err) {
            console.error(err);
            return false;
        }
    }
    public async Login(login: string, password: string) : Promise<{token: string}> {
        if (login == this.configService.get<string>('AdminLogin') && password == this.configService.get<string>('AdminPassword')) {
            return {
                token: this.GetToken(login, password)
            }
        }
        return null;
    }
}