export default `<!DOCTYPE html>
<html>
    <head>
        <style>
.container {
    width: 600px;
    background: #EEEEEE;
    font-family: Roboto;
    margin: 0 auto;
}
.top-title-container {
    padding: 28px 0 24px;
    background: #1F2633;
}
.top-title {
    width: 252px;
    margin: 0 auto;
}
.hands {
    width: 70px;
    margin: 10px auto 0;
}
.became-partner {
    font-size: 26px;
    font-weight: 500;
    width: 256px;
    line-height: 34px;
    margin: 30px auto 0;
}
img {
    display: block;
}
.your-link {
    font-size: 14px;
    line-height: 19px;
    margin: 26px auto 0;
    text-align: center;
}
.link-container {
    width: 391px;
    height: 54px;
    display: flex;
    justify-content: space-between;
    border: 1px solid #707070;
    background: white;
    padding: 0 11px;
    margin: 20px auto 0;
    border-radius: 4px;
    font-size: 23px;
    font-weight: bold;
    color: black;
}
.share-link {
    margin: 22px auto 0;
    text-align: center;
    width: 414px;
    font-size: 21px;
}
.explanation {
    margin: 22px auto 0;
    width: 537px;
    font-size: 14px;
}
.footer {
    background: #1F2633;
    margin: 28px auto 0;
    width: 100%;
    padding-top: 15px;
    padding-bottom: 16px;
}
.footer .flex {
    position: relative;
    margin-left: 31px;
    margin-right: 31px;
}
.footer .images .logo {
    width: 120px;
    height: 19px;
    display: inline-block;
}
.footer .images .socials {
    width: 75px;
    display: inline-block;
    margin-left: 339px;
}
.footer .images .socials .icon {
    width: 16px;
    height: 16px;
    cursor: pointer;
    font-size: 0;
    display: inline-block;
    margin-left: 5px;
}
.footer .images .socials .icon.vk {
    width: 20px;
    background: url('https://betmax.ru/img/email/vk.png');
    background-size: 20px;
    background-position: 0 center;
}
.footer .images .socials .icon.vk:hover {
    background: url('https://betmax.ru/img/email/vk-green.png');
    background-size: 20px;
    background-position: 0 center;
}
.footer .images .socials .icon.tg {
    background: url('https://betmax.ru/img/email/telegram.png');
    background-size: 16px;
}
.footer .images .socials .icon.tg:hover {
    background: url('https://betmax.ru/img/email/telegram-green.png');
    background-size: 16px;
}
.footer .images .socials .icon.instagram {
    background: url('https://betmax.ru/img/email/instagram.png');
    background-size: 16px;
}
.footer .images .socials .icon.instagram:hover {
    background: url('https://betmax.ru/img/email/instagram-green.png');
    background-size: 16px;
}
.footer .text {
    margin-top: 6px;
}
.footer .text div, .footer .text a {
    margin: auto 0;
}
.footer .text .we-are-not-bk {
    font-size: 8px;
    color: white;
    display: inline-block;
}
.footer .text .feedback {
    color: #83CA10;
    font-size: 12px;
    font-weight: 500;
    text-decoration: none;
    margin-left: 277px;
}
.footer .you-get-this-mail, .footer .copyright {
    font-size: 8px;
    color: white;
    opacity: 0.5;
    text-align: center;
    margin-top: 15px;
}
.footer .you-get-this-mail a, .footer .copyright a {
    color: white;
}
.footer .copyright {
    margin-top: 8px;
}
        </style>
    </head>
    <body>
        <div class="container">
            <div class="top-title-container">
                <img class="top-title" src="https://betmax.ru/img/email/logo-shadow.png">
            </div>
            <img class="hands" src="https://betmax.ru/img/email/hands.png">
            <div class="became-partner">Вы стали партнёром расширения BetMAX</div>
            <div class="your-link">Ваша партнерская ссылка:</div>
            <input class="link-container" value="%referal_link%">
            <div class="share-link">Делитесь вашей ссылкой с другими игроками и получайте <b>%install_price%₽</b> за каждого нового привлеченного пользователя*</div>
            <div class="explanation">*После установки BetMAX по реферальной ссылке, пользователю, установившему расширение, необходимо выполнить сравнение коэффициентов не менее 100 раз. После этого установка будет засчитываться партнёру.</div>
            <div class="footer">
                <div class="images flex">
                    <img class="logo" src="https://betmax.ru/img/logo.png">
                    <div class="socials">
                        <a href="https://vk.com/betmax_ru" target="_blank" class="vk icon"></a>
                        <a href="https://t.me/betmax_ru" target="_blank" class="tg icon"></a>
                        <a href="https://www.instagram.com/betmax_ru/" target="_blank" class="instagram icon"></a>
                    </div>
                </div>
                <div class="text flex">
                    <div class="we-are-not-bk">Мы не являемся букмекерской конторой<br>и не принимаем ставки на спорт.</div>
                    <a class="feedback" href="mailto:admin@betmax.ru">Связаться с нами</a>
                </div>
                <div class="you-get-this-mail">Вы получили данное письмо, потому что зарегестрировались в партнерской программе расширения BetMAX.<br>Письмо сформировано автоматически. Если у Вас возникнут какие-либо вопросы, напишите в нашу службу поддержки info@betmax.ru</div>
                <div class="copyright">© 2019–%current_year% betmax.ru</div>
            </div>
        </div>
    </body>
</html>`