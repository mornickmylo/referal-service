import { BadRequestException, Body, Controller, Get, Post, Query, Req } from '@nestjs/common';
import { Request } from 'express';
import axios from 'axios';
import { ConfigService } from '@nestjs/config';
import { LogItem } from './data-types';
import { Cron } from '@nestjs/schedule';

@Controller('server-cf-stats')
export class ServerCfDelayStatsController {
    private statsStorage: {[bkId: string]: [number, number][]} = {};
    private statsPushed = 0;

    constructor(private configService: ConfigService) {}

    @Post('push')
    async Push(@Body() data: {[bkId: string]: {timestamp: number, avgValue: number}}) {
        for (const bkId in data) {
            const bkKey = `${bkId}_avg_delay`;
            if (!this.statsStorage[bkKey]) {
                this.statsStorage[bkKey] = [];
            }
            this.statsStorage[bkKey].push([(data[bkId].timestamp / 1000), data[bkId].avgValue]);
            this.statsPushed++;
        }
        if (this.statsPushed > parseInt(this.configService.get<string>('MaxServerCfDelayStats'))) {
            await this.Send();
        }
    }

    @Cron('0 * * * * *')
    async Send() {
        if (this.statsPushed > 0) {
            try {
                await axios.post(this.configService.get<string>('CfDelayStatsReceivingURL'), this.statsStorage);
            }
            catch (e) {
                console.error('An error occuried on post to CfDelayStatsReceivingURL');
                console.error(this.statsStorage);
            }
            this.statsStorage = {};
            this.statsPushed = 0;
        }
    }
}