import { Controller, Post, Headers, NotFoundException, UnauthorizedException, Body } from '@nestjs/common';
import ReferralDb from './referral-db';
import CodesService from './code-service';
import UserController from './user-controller';
import { ConfigService } from '@nestjs/config';
import Mailer from './mailer';
import { LoginConfirm,  Promocode,  ReferalWithStats, ReferralDataWithStats, User, UserReceivingEnd, UserShort, UserShortlWithCodes, WithdrawalRequest } from './data-types';
import mailTemplate from './mail-template';

@Controller()
export class ReferalController extends UserController {
    constructor(private referralDB: ReferralDb, codesService: CodesService, configService: ConfigService, mailer: Mailer) {
        super(referralDB, codesService, configService, mailer);
    }
    protected GetCodeMailText(): string {
        return 'Ваш код подтверждения для партнерской программы BetMAX'
    }

    @Post('get-referral-data')
    async GetReferralData(@Headers('Authorization') token: string) : Promise<ReferralDataWithStats> {
        const userId = await this.Authorize(token);
        if (userId) {
            const {link, balance} = await this.referralDB.GetLinkAndBalance(userId);
            const withdrawalHistory = await this.referralDB.GetWithdrawalHistory(userId);
            const stats = await this.referralDB.GetStats(userId);
            const purchaseStats = await this.referralDB.GetPurchaseStats(userId);
            const promocode = await this.referralDB.GetPromocode(userId);
            return {link, balance, ...stats, purchases: purchaseStats, withdrawalHistory, promocode};
        }
        throw new UnauthorizedException();
    }
    @Post('withdrawal')
    async Withdrawal(@Body() request: WithdrawalRequest) : Promise<number> {
        const user = await this.referralDB.GetBy(request.phone, UserReceivingEnd.Phone);
        if (user) {
            const {balance} = await this.referralDB.GetLinkAndBalance(user.id);
            await this.referralDB.ChangeBalance(user.id, request.sum, '-');
            let requestId = await this.referralDB.InsertWithdrawalRequest(user.id, request.sum, request.account, request.accountType);
            requestId = requestId * 931;
            await this.mailer.Send(user.email, "BetMAX: новая заявка на вывод денежных средств", `Ваша заявка №${requestId} на вывод ${request.sum} ₽ принята к рассмотрению.`, `Ваша заявка №${requestId} на вывод ${request.sum} ₽ принята к рассмотрению.`);
            return balance - request.sum;
        }
        throw new NotFoundException();
    }

    @Post('login-confirm')
    async ReferralLoginConfirm(@Body() data: LoginConfirm) : Promise<{data: ReferalWithStats, token: string}> {
        const loginResult = await super.LoginConfirm(data);
        const {link, balance} = await this.referralDB.GetLinkAndBalance(loginResult.data.id);
        const withdrawalHistory = await this.referralDB.GetWithdrawalHistory(loginResult.data.id);
        const stats = await this.referralDB.GetStats(loginResult.data.id);
        const purchaseStats = await this.referralDB.GetPurchaseStats(loginResult.data.id);
        const referal: ReferalWithStats = {
            ...loginResult.data,
            link,
            balance,
            withdrawalHistory,
            ...stats,
            purchases: purchaseStats
        }
        return {data: referal, token: loginResult.token};
    }

    @Post('install-price')
    async GetInstallPrice() : Promise<number> {
        return await this.referralDB.GetInstallPrice();
    }

    @Post('upsert')
    async UpsertReferral(@Headers('X-Forwarded-For') clientIp: string, @Body('data') data: UserShortlWithCodes, @Body('newData') newData?: UserShort) : Promise<{data: ReferalWithStats | UserShortlWithCodes, token?: string}> { 
        const result = await super.Upsert(clientIp, data, newData);
        if (result.resultType == 'register') {
            const newUser: ReferalWithStats = {
                ...(result.data as User),
                clicks: [],
                installs: [],
                link: `https://betmax.ru/ref/${await this.CreateReferalSuffix()}`,
                balance: 0,
                withdrawalHistory: [],
                purchases: []
            }
            await this.SendWelcomeEmail(newUser.link, newUser.email);
            const promocode: Omit<Promocode, 'id'> = {
                referralId: result.newUserId.toString(),
                code: this.GenerateRandomStr(10).toUpperCase(),
                discount: 10,
                referralIncome: 20,
                trialLengthIncrease: 0, 
                maxApplies: 0
            };
            await this.referralDB.InsertPromocode(promocode);
            return {
                data: newUser,
                token: result.token
            }
        } else {
            return {
                data: result.data as UserShortlWithCodes,
                token: result.token
            }
        }
    }


    protected async NotifyAdminAboutRegister(ip: string, data: UserShort) : Promise<void> {
        const body = `IP: ${ip}`+
            `Имя: ${data.name}`+
            `E-mail: ${data.email}`+
            `Телефон: +${data.phone}`;
        await this.mailer.Send(this.configService.get<string>('AdminEmail'), 'Новая регистрация в партнерке', body, body);
    }
    protected async SendWelcomeEmail(referalLink: string, email: string) : Promise<void> {
        const installPrice = await this.referralDB.GetInstallPrice();
        const mailHtmlBody = mailTemplate
            .replace('%referal_link%', referalLink)
            .replace('%install_price%', installPrice.toString())
            .replace('%current_year%', new Date().getFullYear().toString());
        await this.mailer.Send(email, 'Партнерская программа BetMAX', `Спасибо, что приняли участие в партнёрской программе BetMAX. Ваша персональная ссылка: ${referalLink}`, mailHtmlBody);
    }
    private async CreateReferalSuffix() {
        let suffix = '';
        do {
            suffix = this.GenerateRandomStr(6);
        } while (await this.referralDB.CheckLinkExists(suffix));
        return suffix;
    }
    GenerateRandomStr(length: number) {
        let suffix = '';
        const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        for (let i = 0; i < length; i++) {
            suffix += characters.charAt(Math.floor(Math.random() * characters.length));
        }
        return suffix;
    }
}