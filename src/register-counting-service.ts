import { Injectable, Logger } from '@nestjs/common';
import { Cron } from '@nestjs/schedule';
import { from } from 'rxjs';
import Db from './db';
import { LogItem } from './data-types';
import axios from 'axios';
import Mailer from './mailer';
import ReferralDb from './referral-db';
import { ConfigService } from '@nestjs/config';


@Injectable()
export default class RegisterCountingService {

    constructor(private db: ReferralDb, private configService: ConfigService, private mailer: Mailer) {}
    
    @Cron('45 * * * * *')
    async calcConfirmedUsers() {
        const logRows = await this.GetLastLogRows();
        const unconfirmedUsers = await this.db.GetUnconfirmedUserId();
        const unconfirmedUsersActionCounts = await this.db.GetActionsCounts(unconfirmedUsers);
        unconfirmedUsers.forEach(uu => {
            if (unconfirmedUsersActionCounts[uu] === undefined) {
                unconfirmedUsersActionCounts[uu] = 0;
            }
        });
        const actions = this.MapRows(logRows);
        const usersWhoTookAction : Set<string> = new Set();
        actions.forEach(action => {
            if (unconfirmedUsers.includes(action.userId)) {
                unconfirmedUsersActionCounts[action.userId]++;
                usersWhoTookAction.add(action.userId);
            }
        });
        for (const userId in unconfirmedUsersActionCounts) {
            if (unconfirmedUsersActionCounts[userId] >= 100) {
                await this.db.ConfirmInstall(userId);
            }
        }
        for (const userId of usersWhoTookAction) {
            await this.db.SetActionsCount(userId, unconfirmedUsersActionCounts[userId]);
        }
    }

    private async GetLastLogRows(): Promise<string[]> {
        const lastLogRowId = await this.db.GetLastLogRowId();
        const lastLogRowIdParts = lastLogRowId && lastLogRowId.includes('%') ? lastLogRowId.split('%') : [' '];
        let rows: string[] = [];
        for (let i = 100; i<1000; i+=50) {
            const respText = (await axios.get(`${this.configService.get<string>('LoggingServiceUrl')}?n=${i}`)).data as string;
            rows = respText.split('\n').filter(r => !!r.trim());
            const lastHandledRowIndex = rows.findIndex(row => lastLogRowIdParts.every(part => row.includes(part)));
            if (lastHandledRowIndex == -1 && i == 950)
            if (lastHandledRowIndex != -1) {
                return this.HandleRowsBunch(rows, lastHandledRowIndex);
            }
        }
        return this.HandleRowsBunch(rows, -1);
    }

    public async GetLogRowsFrom(date: Date) : Promise<LogItem[]> {
        let rows: string[] = [];
        for (let i = 10000; i<1000000; i+=10000) {
            const respText = (await axios.get<string>(`${this.configService.get<string>('LoggingServiceUrl')}?n=${i}`)).data;
            rows = respText.split('\n').filter(r => !!r.trim());
            const earliestRow = rows.length && this.MapRow(rows[rows.length - 1]);
            const isNeededDateAchieved = earliestRow && new Date(earliestRow.updated_at) <= date;
            if (isNeededDateAchieved) {
                break;
            }
        }
        for (let i = 1; i<=rows.length; i+=1) {
            const currentRowItem = rows[rows.length - i] && this.MapRow(rows[rows.length - i]);
            if (currentRowItem && new Date(currentRowItem.updated_at) >= date) {
                return this.MapRows(rows.slice(0, i+1));
            }
        }
        return [];
    }

    private async HandleRowsBunch(rows: string[], lastHandledRowIndex: number) : Promise<string[]> {
        const lastRow = rows[rows.length - 1];
        const timeUserIdMatch = lastRow.match(/(?<time>\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2})\s+?(?<userId>[A-Za-z0-9-]+)\s+?(?<ip>[0-9\.]+?)\s+?(?<action>[a-z_]+?)\s/);
        const updated_atMatch = lastRow.match(/updated_at:([\d\.]+)/);
        const newLastLogRowId = updated_atMatch ? `${timeUserIdMatch.groups.userId}%${updated_atMatch[1]}` : `${timeUserIdMatch.groups.userId}%${timeUserIdMatch.groups.action}%${timeUserIdMatch.groups.time}`;
        await this.db.SetLastLogRowId(newLastLogRowId);
        return rows.slice(lastHandledRowIndex+1);
    }

    private MapRows(rows: string[]) : LogItem[] {
        return rows.map(row => this.MapRow(row)).filter(r => !!r);
    }

    private MapRow(row: string) : LogItem {
        let match = row.match(/(?<time>\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2})\s+?(?<userId>[A-Za-z0-9-]+)\s+?(?<ip>[0-9\.]+?)\s+?(?<action>[a-z_]+?)\s+?(?<bk_name>[a-z0-9]+?)\s+?(?<bk_event_id>[A-Z0-9]+?)\s+?request_id:(?<request_id>.+?)\s+?bet:(?<bet>.+?)\s+?prematch:(?<prematch>[01])\s+?created_at:(?<created_at>[\d\.]+?)\s+?updated_at:(?<updated_at>[\d\.]+)/);
        if (match) {
            return {
                date: new Date(match.groups.time),
                userId: match.groups.userId,
                ip: match.groups.ip,
                action: match.groups.action,
                bk_name: match.groups.bk_name,
                bk_event_id: match.groups.bk_event_id,
                request_id: match.groups.request_id,
                bet: match.groups.bet,
                prematch: !!parseInt(match.groups.prematch),
                created_at: new Date(parseFloat(match.groups.created_at)*1000),
                updated_at: new Date(parseFloat(match.groups.updated_at)*1000)
            }
        }
        match = row.match(/(?<time>\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2})\s+?(?<userId>[A-Za-z0-9-]+)\s+?(?<ip>[0-9\.]+?)\s+?(?<action>[a-z_]+)/);
        if (match) {
            return {
                date: new Date(match.groups.time),
                userId: match.groups.userId,
                ip: match.groups.ip,
                action: match.groups.action,
                bk_name: '',
                bk_event_id:'',
                request_id: '',
                bet: '',
                prematch: false,
                created_at: new Date(match.groups.time),
                updated_at: new Date(match.groups.time)
            }
        }
        match = row.match(/(?<time>\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2})\s+?(?<ip>[0-9\.]+?)\s+?start\s+?cmd:start\s+?settings_data:(?<userId>[A-Za-z0-9-]+)\|/);
        if (match) {
            return {
                date: new Date(match.groups.time),
                userId: match.groups.userId,
                ip: match.groups.ip,
                action: 'start',
                bk_name: '',
                bk_event_id:'',
                request_id: '',
                bet: '',
                prematch: false,
                created_at: new Date(match.groups.time),
                updated_at: new Date(match.groups.time)
            }
        }
        console.log('no complete match');
        console.log(row);
        return null;
    }

    @Cron('0 0 14 * * *')
    async IsLoggingServiceUp(mailFail: boolean = true) : Promise<boolean> {
        const resp = await axios.get<string>(`${this.configService.get<string>('LoggingServiceUrl')}?n=100`);
        const rows = resp.data.split('\n').filter(r => !!r.trim());
        if (resp.status != 200 || !rows.length) {
            if (mailFail) {
                await this.mailer.Send(this.configService.get<string>('AdminEmail'), 'Сервис логирования не работает', 'Сервис логирования не работает', 'Сервис логирования не работает');
            }
            return false;
        }
        return true;
    }
}