import { Test, TestingModule } from '@nestjs/testing';
import { ReferalController } from './referal-controller';
import ProUserDb from './pro-user-db';
import { ConfigService } from '@nestjs/config';
import UserPhoneCheckingService from './user-phone-checking-service';
import DbConnectionProvider from './db-connection-provider';

describe('UserPhoneCheckingService', () => {
    let db: ProUserDb;
    let testedService: UserPhoneCheckingService;
    let configService: ConfigService;
    let userData: {trialAvailable: boolean, activeUntil: Date, phoneCheckFails: number, needLogout: boolean} = {
        trialAvailable: false, 
        activeUntil: new Date(),
        phoneCheckFails: 0,
        needLogout: false
    }

    beforeEach(async () => {
        const app: TestingModule = await Test.createTestingModule({
            providers: [ProUserDb, ConfigService, UserPhoneCheckingService, DbConnectionProvider],
        }).compile();

        db = app.get<ProUserDb>(ProUserDb);
        testedService = app.get<UserPhoneCheckingService>(UserPhoneCheckingService);
        configService = app.get<ConfigService>(ConfigService);
        jest.spyOn(db, 'GetProUserData').mockImplementation(async () => userData);
        jest.spyOn(db, 'SetProUserData').mockImplementation(async (userId: string, trialAvailable: boolean, activeUntil: Date, phoneCheckFails: number, needLogout: boolean) => {
            userData.activeUntil = activeUntil;
            userData.trialAvailable = trialAvailable;
            userData.phoneCheckFails = phoneCheckFails;
            userData.needLogout = needLogout;
        });
        jest.spyOn(db, 'GetUsersToCheckPhones').mockImplementation(async () => '1');
    });

    describe('CheckPhones', () => {
        it('should increase fails amount', async () => {
            await testedService.SetCheckPhoneResult('1', false);
            expect(userData.phoneCheckFails).toBe(1);
        });

        it('should set needLogout to true when fail limit is reached', async () => {
            userData.phoneCheckFails = 0;
            userData.needLogout = false;
            for (let  i = 0; i < parseInt(configService.get<string>('PhoneCheckFailLimit')); i++) {
                await testedService.SetCheckPhoneResult('1', false);
            }
            expect(userData.needLogout).toBe(true);
            expect(userData.phoneCheckFails).toBe(0);
        });
    });
});
