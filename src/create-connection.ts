import { ConfigService } from "@nestjs/config";
import { createConnection as nativeCreateConnection } from "mysql2/promise";

export const createConnection = (configService: ConfigService) => nativeCreateConnection({
    charsetNumber: 45,
    host: configService.get<string>('DBHost'),
    port: parseInt(configService.get<string>('DBPort')),
    user: configService.get<string>('DBUser'),
    database: configService.get<string>('DBName'),
    password: configService.get<string>('DBPassword')
})