import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Cron } from '@nestjs/schedule';
import Db from './db';
import ProUserDb from './pro-user-db';
import axios from 'axios';

@Injectable()
export default class UserPhoneCheckingService {

    constructor(private db: ProUserDb, private configService: ConfigService) {}

    @Cron('0 0 10,14,18 * * *')
    public async CheckPhones() {
        const usersToCheck = await this.db.GetUsersToCheckPhones(new Date());
        if (usersToCheck) {
            for (const userId in usersToCheck.split(',')) {
                await this.CheckUser(userId);
            }
        }
    }

    public async CheckUser(userId: string) : Promise<void> {
        const userData = await this.db.GetBy(userId, 'id');
        const requestUrl = this.configService.get<string>('HLRServiceUrl').replace('%phone%', userData.phone).replace('%userId%', userId);
        await axios.get(requestUrl);
    }

    public async SetCheckPhoneResult(userId: string, checkResult: boolean) {
        const userData = await this.db.GetProUserData(userId);
        if (!checkResult) {
            const needLogout = userData.phoneCheckFails + 1 >= parseInt(this.configService.get<string>('PhoneCheckFailLimit'));
            await this.db.SetProUserData(userId, userData.trialAvailable, userData.activeUntil, needLogout ? 0 : userData.phoneCheckFails + 1, needLogout);
        } else {
            if (userData.phoneCheckFails != 0) {
                await this.db.SetProUserData(userId, userData.trialAvailable, userData.activeUntil, 0, userData.needLogout);
            }
        }
    }
}