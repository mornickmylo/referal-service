import {createTransport} from 'nodemailer';
import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export default class Mailer {
    constructor(private configService: ConfigService) {}
    public async Send(to: string, subject: string, text: string, html: string) {
        let transporter = createTransport({
            host: this.configService.get<string>('SMTPHost'),
            port: parseInt(this.configService.get<string>('SMTPPort')),
            secure: true, // true for 465, false for other ports
            auth: {
              user: this.configService.get<string>('SMTPLogin'), // generated ethereal user
              pass: this.configService.get<string>('SMTPPassword'), // generated ethereal password
            },
        });
        let info = await transporter.sendMail({
            from: this.configService.get<string>('SMTPFrom'), // sender address
            to, // list of receivers
            subject, // Subject line
            text, // plain text body
            html // html body
        });
        console.log("Message sent: %s", info.messageId);
    }
}