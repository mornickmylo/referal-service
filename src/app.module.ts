import { Module, Scope, FactoryProvider } from '@nestjs/common';
import { ReferalController } from './referal-controller';
import { WithdrawalController } from './withdrawal-controller';
import { LoggingController } from './logging-controller';
import CodesService from './code-service';
import { ScheduleModule } from '@nestjs/schedule';
import RegisterCountingService from './register-counting-service';
import ReferralDb from './referral-db';
import ProUserDb from './pro-user-db';
import { ProUserController } from './pro-user-controller';
import { ConfigModule, ConfigService } from '@nestjs/config';
import Mailer from './mailer';
import AdminService from './admin-service';
import ProUsersController from './pro-users-controller';
import UserPhoneCheckingService from './user-phone-checking-service';
import ReceiptService from './receipt-service';
import DbConnectionProvider from './db-connection-provider';
import { ServerCfDelayStatsController } from './server-cf-delay-stats-controller';

@Module({
    imports: [
        ScheduleModule.forRoot(),
        ConfigModule.forRoot()
    ],
    controllers: [ReferalController, WithdrawalController, LoggingController, ProUserController, ProUsersController, ServerCfDelayStatsController],
    providers: [ReferralDb, ProUserDb, CodesService, RegisterCountingService, Mailer, AdminService, UserPhoneCheckingService, ReceiptService, DbConnectionProvider],
})

export class AppModule { }
