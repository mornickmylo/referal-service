import { Body, Post, Headers, ConflictException, NotFoundException, UnauthorizedException, InternalServerErrorException, Header } from '@nestjs/common';
import { LoginConfirm, ReferalWithStats, UserShort, UserShortlWithCodes, UserReceivingEnd, WithdrawalRequest, UserWithId, User } from './data-types';
import CodesService from './code-service';
import Mailer from './mailer';
import {sign, verify} from 'jsonwebtoken';
import Db from './db';
import { ConfigService } from '@nestjs/config';

export default abstract class UserController {
    constructor(private db: Db, private codesService: CodesService, protected configService: ConfigService, protected mailer: Mailer) {}

    @Post('send-code')
    async SendCode(@Body('type') type: UserReceivingEnd, @Body('value') value: string) : Promise<string> {
        const senders : {[receiver in UserReceivingEnd] : (address: string) => Promise<string> } = {
            'phone': (address: string) => this.codesService.SendToPhone(address),
            'email': (address: string) => this.codesService.SendToEmail(address, this.GetCodeMailText()),
        };
        const code = await senders[type](value);
        if (code) {
            await this.db.SetCode(type, code, value);
            return code;
        } else {
            throw new InternalServerErrorException(`Ошибка при отправке кода подтверждения на ${value}. Обратитесь в поддержку`);
        }
    }

    @Post('login')
    async Login(@Body('login') login: string) : Promise<{code: string, type: UserReceivingEnd}> {
        let type : UserReceivingEnd = UserReceivingEnd.Phone;
        let user = await this.db.GetBy(login, type);
        if (!user) {
            type = UserReceivingEnd.Email;
            user = await this.db.GetBy(login, type);
        }
        if (!user) {
            throw new NotFoundException();
        }
        const senders : {[receiver in UserReceivingEnd] : (address: string) => Promise<string> } = {
            'phone': (address: string) => this.codesService.SendToPhone(address),
            'email': (address: string) => this.codesService.SendToEmail(address, this.GetCodeMailText()),
        }
        const code = await senders[type](user[type]);
        if (code) {
            await this.db.SetCode('unknown', code, login);
            return {code, type};
        } else {
            throw new InternalServerErrorException(`Ошибка при отправке кода подтверждения на ${user[type]}. Обратитесь в поддержку`);
        }
    }

    async Upsert(clientIp: string, data: UserShortlWithCodes, newData?: UserShort) : Promise<{data: User | UserShortlWithCodes, token?: string, resultType: 'edit' | 'register', newUserId?: number}> {
        const email = newData ? newData.email : data.email, phone = newData ? newData.phone : data.phone
        const storedCodes = await this.db.GetCodes(phone, email);
        if (!storedCodes || Object.entries(data.codes).some(([type, code]) => code != storedCodes[type])) {
            console.log('Upsert: codes incorrect');
            Object.entries(data.codes).forEach(([type, code]) => console.log(`${type} codes is ${code}, should be ${storedCodes[type]}`));
            throw new UnauthorizedException();
        }
        if (newData) {
            await this.db.EditUser(data.phone, newData);
            return {data: {
                ...data,
                ...newData
            },
            resultType: 'edit'};
        } else {
            const conflictField = await this.GetTakenUserData(data);
            if (conflictField) {
                throw new ConflictException(`${conflictField} taken`);
            }
            const newUser: User = {
                ...data,
                registered: new Date().toISOString(),
                confirmed: true
            }
            const newUserId = await this.db.CreateUser(newUser);
            await this.NotifyAdminAboutRegister(clientIp, newUser);
            return {data: newUser, token: this.GetToken(newUserId.toString()), resultType: 'register', newUserId};
        }
    }

    async LoginConfirm(@Body() data: LoginConfirm) : Promise<{data: UserWithId, token: string}> {
        const code = await this.db.GetCode('unknown', data.login);
        if (code && code == data.code) {
            const {user, codeType} = await this.db.FindByUserReceivingEnd(data.login);
            if (user) {
                return {data: user, token: this.GetToken(user.id)};
            }
            console.log(404);
            throw new NotFoundException();
        }
        console.log(401);
        throw new UnauthorizedException();
    }
    
    @Post('can-register')
    async CanRegister(@Body() request: UserShort) : Promise<{result: string, reason?: string}> {
        const conflictField = await this.GetTakenUserData(request);
        if (conflictField) {
            throw new ConflictException(`${conflictField} taken`);
        }
        return {result: 'ok'};
    }

    @Post('get-data')
    async GetData(@Headers('Authorization') token: string) : Promise<UserWithId> {
        const userId = this.Authorize(token);
        if (userId) {
            const user = await this.db.GetBy(userId, 'id');
            if (user) {
                return user;
            }
            throw new NotFoundException();
        }
        throw new UnauthorizedException();
    }
    private GetToken(id: string) {
        return sign({id}, this.configService.get<string>('TokenSign'));
    }
    protected Authorize(token: string) : string {
        try {
            const data = verify(token, this.configService.get<string>('TokenSign')) as {id: string};
            return data.id;
        } catch(err) {
            console.error(err);
            return '';
        }
    }
    private async GetTakenUserData(request: UserShort) : Promise<string> {
        const phoneUser = await this.db.GetBy(request.phone, UserReceivingEnd.Phone);
        if (phoneUser) {
            return 'phone';
        }
        const emailUser = await this.db.GetBy(request.email, UserReceivingEnd.Email);
        if (emailUser) {
            return 'email';
        }
        return '';
    }
    protected abstract NotifyAdminAboutRegister(ip: string, data: UserShort) : Promise<void>;

    protected abstract GetCodeMailText() : string;
}